//import the contents of the Express pacakage to use for our application
const express = require("express");

//Mongoose is an ODM (Object Document Mapper) library for MongoDB and Node.js that manages models/schemas and allows a quick and easy way to connect to a MongoDB database
const mongoose = require("mongoose");

//give the express() function from the Express package a variable "app" so that it can be called more easily
const app = express();

//Mongoose's connect method takes our MongoDB Atlas connection string and uses it to connect to Atlas and authenticate our credentials, as well as specifices the database that our app needs to use

//useNewUrlParser and useUnifiedTopology are both set to true as part of a newer Mongoose update that allows a more efficient way to connect to Atlas, since the the older way is about to be deprecated (become obsolete)
mongoose.connect("mongodb+srv://admin:admin@testdatabase1.elvgf.mongodb.net/B153_tasks?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//confirm that Atlas connection is successful
//mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))
//mongoose.connection is used to handle error or succesful connection to Mongodb
let db = mongoose.connection;
//check mongoose.connection on error
//console.error.bind() will allow us to display the error of mongoose.connection both in your terminals and in our browser console.
db.on("error", console.error.bind(console, "connection error"));

//confimation that connection to mongodb is successful.
db.once("open",()=>console.log("We're now connected to MongoDB"));

//middleware - are functions/methods we use in our server/api
//this middleware functions serve as gates/it allows us do other tasks:
//This will allow us to handle the request body json
app.use(express.json());

//declare a port number variable
const port = 4000;

//create a GET route to check if Express is working
app.get("/", (req, res) => {
	res.send("Hello from Express!")
})

/*
	Mongoose Schema

	Before we can create documents in our database, we first have to declare a "blueprint" or "template" of our documents. This is to ensure that the content/fields of documents are uniform. Gone are the days when we have to worry if we correctly spelled the fields in our documents.

	Schema acts a blueprint of our data/document.

	It is a representation of how our documents is structured. It also determines the types of data and the expected fields/properties per document.

*/

//Schema() is a constructor from mongoose that will allow us to create a new schema object.

const taskSchema = new mongoose.Schema({

	/*
		Define the fields for the task document.

		The task document should have a name and status field.

		Both fields MUST be strings.

	*/

	name: String,
	status: String

})

/*
sample task document based on schema

	{
		name: "Sample Task",
		status: "Complete"
	}

*/

/*
	Mongoose Model

	Models are used to connect you api to the corresponding collection in your database. It is a representation of your documents.

	Models uses schemas to create objects/documents that corresponds to the schema. By default, when creating the collection from your model, the collection name is pluralized.

	mongoose.model(<nameOfCollection>,<schemaToFollow>)

*/

let Task = mongoose.model("Course",taskSchema);

//mongodb equivalent of Task model = db.tasks

//POST route - add task documents:e
app.post('/',(req,res)=>{

	//check the incoming request body
	// console.log(req.body)

    //req.body in terminal:
    //{name: "Learn NodeJS", status: "pending"}


    //Create a new task object/document from ou model
    let newTask = new Task({
        name: req.body.name,
        status: req.body.status
    })

    //newTask is a mongoose document/opbject
    //console.log(newTask)

    //save() is a method from an object/document created by a model.
    //this method will allow us to save
    //.then() and catch() chain
    /*
    
        .then() is used to handle the result/return of a function. If the method/function properly returns a value, we can run a seperate function to handle it.

        .catch() is used to catch/handle an error. If there is an error, we will handle it in a separate function aside from our result
    
    */


    newTask.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//GET Route - get all task documents

app.get('/tasks', (req,res)=>{

    //res.send("Testing from get all task documents route")

    //to be able to find() or get all documents from a collection, we will use the find() method of our model.
    //mongoDB equivalent - db.tasks.find({})
    Task.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))

})








//Activity

const userSchema = new mongoose.Schema({

	username: String,
	password: String

})

let User = mongoose.model("User",userSchema);

app.post('/users',(req1,res1)=>{

    let newUser = new User({
        username: req1.body.username,
        password: req1.body.password
    })


    newUser.save()
    .then(result => res1.send(result))
    .catch(error => res1.send(error))
})


app.get('/users', (req1,res1)=>{

    User.find({})
    .then(result => res1.send(result))
    .catch(error => res1.send(error))

})



//start the server and confirm that it is running
app.listen(port, () => console.log(`Server running at port ${port}`))





















